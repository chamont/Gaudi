!-----------------------------------------------------------------------------
! Package     : GaudiMTTools
! Responsible : Illya SHAPOVAL
! Purpose     : The package for Gaudi multithreading tools and machinery
!-----------------------------------------------------------------------------

================================ Gaudi v28r1 =================================

! 2016-12-14 - Attila Krasznahorkay (commit a42ff2a)

 - CMake configuration changes to build Gaudi with optional externals missing

   In the last days I experimented a bit with teaching Gaudi how to build when
   only ROOT, Boost and TBB are available to it. This is the result. As far as I
   can tell the code builds in the same way as the current master branch when
   all possible externals are available. (All the ones available in a full ATLAS
   offline build at least.) And I'm also able to build the project in these
   modes:
     * On Ubuntu 16.04 with the system provided Boost and TBB versions, against
   a privately built ROOT 6 version.
     * On macOS Sierra against privately built Boost, ROOT and TBB versions.
   Both when building the project completely on its own, and when building it
   against an ATLAS externals project.
    Some notes:
     * The code uses C++14 expressions in quite a few places by now. So I raised
   the default C++ standard to C++14. This is the default in ATLAS builds since
   a while, I only saw this as an issue when building Gaudi "on its own" with a
   very minimal configuration command.
     * While the code advertises that it still has support for ROOT 5, it's not
   able to build against it since a wile. Since the updates I put in for TLS
   variables on macOS. (The macros used there are ROOT 6 only.) And this update
   makes things even worse. As the code now relies on not providing the
   definition of some classes to the dictionary generator in GaudiPython that
   are not available. (Because AIDA or XercesC is missing.) While ROOT 6 handles
   this with just a build warning, ROOT 5's genreflex treats this with an ERROR
   message. That I was unable to get rid of.
    So, extended testing is clearly necessary, the configuration code could
   definitely be fine tuned as I probably turned off the build of more source
   files than absolutely necessary, but I think we should move ahead with such a
   configuration organisation.
    See merge request !241

================================ Gaudi v28r0 =================================

! 2016-10-27 - Gerhard Raven, Marco Clemencic (commit b111898)

 - modernization of Property

   This is a major rewrite of the system of `Property` classes. Started as  the
   implementation of GAUDI-1214, it continued as a review of
   `IProperty` and `PropertyMgr`, to end up in a complete rewrite
   (and optimization) of `PropertyWithValue`.

   * Fixes GAUDI-1214
    - added missing `declareProperty` signature
    - added `PropertyHolder` (an updated `PropertyMgr`)
    - adapted all properties in Gaudi
   * improved use of `PropertyHolder`
    - use inheritance instead of composition
    - removed ~200 (duplicated) lines of code in GaudiKernel
   * optimization of Property (fixes GAUDI-1229)
    - use templates and automatic code generation to handle efficiently
      both value and reference properties
      - avoid creation of values on the heap (see GAUDI-1229)
      - removed the *owned* boolean flag
      - improved usability of properties (e.g. begin/end wrappers for
        C++11 loops, increment/decrement operators...)
      - deprecated use of C arrays as properties
    - merged features of `PropertyWithVerifier`, `SimpleProperty` and
      `SimplePropertyRef` into `PropertyWithValue`
    - deduplication of name and doc strings via

   [`boost::string_ref`](http://www.boost.org/doc/libs/1_61_0/libs/utility/doc/html/string_ref.html)

      and a static storage  (see GAUDI-1229)
   * Fixes GAUDI-1205
   * add deprecation warnings in service accessors of `Algorithm` (@graven)
   * renamed `Property` base class to `Gaudi::Details::PropertyBase` and
   `PropertyWithValue` to `Gaudi::Property`
    - added backward compatibility type aliases (but not compatible with
      forward declarations of `class Property`, which should be replaced
      by `#include "GaudiKernel/PropertyFwd.h"`)
   * added macro `GAUDI_PROPERTY_v2` to allow easy implementation of
    backward compatible changes in derived projects (e.g. when user code
    relied on `DoubleProperty` having a verifier)
   * Fixes GAUDI-1268
    The changes are as backward compatible as much as possible (except if you
   explicitly inherit from `SimpleProperty`, or you forward declared
   `class Property`, which now are typedefs), but must be validated in the
   experiment frameworks.
    See merge request !182

! 2016-10-24 - Marco Clemencic (commit a8d6605)

 - hidden/fixed "missing override" warnings exposed after !192

   - warnings from external headers are hidden declaring the include directories
   as `-system`
   - warnings from a couple of new files have been fixed
   - warnings in `HistogramSvc` are hidden because unavoidable (see f83c3d8e)
   - warnings related to CLHEP-136 have been hidden with a special trick (see
   0a238135)
    See merge request !205

! 2016-10-12 - Marco Clemencic (commit b5e05a1)

 - improved handling of package version in CMake configuration

   use project version if a package version is not provided
    See GAUDI-1215.
    See merge request !175

! 2016-10-01 - Marco Clemencic (commit 99b1001)

 - enable missing override warnings (gcc >= 5.1)

   Fixes GAUDI-1241
    See merge request !192

! 2016-07-25 - Marco Clemencic (commit e3d4b07)

 - remove CMT configuration and related files

   * removed CMT configuration files
   * adapted scripts not to use CMT
    Fixes GAUDI-1216 Fixes GAUDI-979
    See merge request !186

! 2016-05-18 - Sami Kama (commit 9eeef96)

 - allow build of Gaudi without LCG and fix ThreadPoolSvc

   Allow building of Gaudi with local externals.
    It also contains a fix to an issue introduced with merge request !160
   (GAUDI-1212).
    Fixes GAUDI-1211, GAUDI-1212.
    See merge request !166

! 2016-04-07 - Hadrien Grasland (commit 0a4087c)

 - Explicitly mark the destructor of GaudiParallelizer as noexcept(true)

   Fixes GAUDI-1187, Gaudi failing to build against recent releases of Intel
   TBB.
    See merge request !132

============================= GaudiMTTools v1r2 ==============================

! 2016-02-11 - commit e2c585c

 - Remove globalTimeOffset getter and GlobalTimeOffset property from GaudiCommon

   GaudiCommon adds a property GlobalTimeOffset and a corresponding getter
   globalTimeOffset to every algorithm/tool which uses GaudiCommon, regardless
   on whether this property is used.

   As there is no code anymore that actually uses globalTimeOffset, this patch
   removes this accessor and the corresponding property.

   Fixes GAUDI-1122.

   See merge request !61

============================= GaudiMTTools v1r1 ==============================

! 2016-01-12 - commit 4dbdc8e

 - fixed clang 3.7 compilation warnings

   Fixes GAUDI-1083.
 See merge request !89

! 2015-11-02 - commit 57f356c

 - Merge branch 'hive' into 'master'

   Fixes GAUDI-978.

   See merge request !65

!======================= GaudiMTTools v1r0 2012-01-27 ============================
! 2012-01-27 - Illya Shapoval
 - Requirements file is fixed: explicit Boost dependecy and linking option is added.

! 2012-01-26 - Illya Shapoval
 - GaudiParallelizer prototype is created. This is an algorithm manager similar
   to GaudiSequencer with the difference in that it runs algorithms in parallel
   for each event. Algorithms being executed in parallel have to be independent
   at this moment (i.e. no input/output data dependencies). Internally
   GaudiParallelizer uses Threadpool 0.2.5 library based on Boost to manage
   thread and task pools.
