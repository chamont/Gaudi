#ifndef GAUDIKERNEL_IDATAHANDLEHOLDER
#define GAUDIKERNEL_IDATAHANDLEHOLDER 1

#include "GaudiKernel/IInterface.h"
#include "GaudiKernel/INamedInterface.h"
#include "GaudiKernel/DataObjID.h"

#include <vector>
#include <set>

namespace Gaudi {
  class DataHandle;
}

class IDataHandleVisitor;

class GAUDI_API IDataHandleHolder : virtual public INamedInterface {
 public:

   virtual std::vector<Gaudi::DataHandle*> inputHandles() const = 0;
   virtual std::vector<Gaudi::DataHandle*> outputHandles() const = 0;

   virtual const DataObjIDColl& extraInputDeps() const = 0;
   virtual const DataObjIDColl& extraOutputDeps() const = 0;

   virtual void acceptDHVisitor(IDataHandleVisitor*) const = 0;

   virtual void commitHandles() = 0;

   virtual const DataObjIDColl& inputDataObjs() const = 0;
   virtual const DataObjIDColl& outputDataObjs() const = 0;

   DeclareInterfaceID(IDataHandleHolder,1,0);

   virtual void declare(Gaudi::DataHandle&) = 0;
   virtual void renounce(Gaudi::DataHandle&) = 0;
};

class IDataHandleVisitor {
public:
  virtual ~IDataHandleVisitor() = default;
  virtual void visit(const IDataHandleHolder*) = 0;
};

#endif // !GAUDIKERNEL_IDATAHANDLEHOLDER
